import java.time.LocalDate;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class WorkingHoursFormView {
	Stage stage;
	WorkingHoursFormController ctrl;
	
	public WorkingHoursFormView(WorkingHoursFormController ctrl, Stage stage) {
		this.stage = stage; 
		this.ctrl = ctrl;
		init();
	}
	
	public void init()  {
		DatePicker datePicker = new DatePicker(); 
		datePicker.setValue(LocalDate.now());
		datePicker.setShowWeekNumbers(true);
		
		Label lblForname = new Label("Vorname"); 
		
		
		Label lblSurname = new Label("Nachname");
		
		GridPane gp = new GridPane();
		gp.add(datePicker, 0, 0);
		gp.add(lblForname, 0, 1);
		gp.add(lblSurname, 1, 1);
		gp.setPadding(new Insets(10));
		
		Scene scene = new Scene(gp, 300, 200); 
		stage.setScene(scene);
		stage.setTitle("Arbeitsstunden Nachweis");
		stage.show();
	}
	
}
