import javafx.stage.Stage;

public class WorkingHoursFormController {
	
	Stage stage;
	
	public WorkingHoursFormController(Stage stage) {
		this.stage = stage;
		showForm();
	}
	
	public void showForm() {
		WorkingHoursFormView formView = new WorkingHoursFormView(this, stage);
	}

}
