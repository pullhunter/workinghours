import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {
	
	public void start(String[] args) throws Exception {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		WorkingHoursFormController ctrl = new WorkingHoursFormController(stage);
		ctrl.showForm();
	}

}
